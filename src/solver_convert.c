#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glpk.h>
#include <float.h>
#include <limits.h>

#define SIZE 100
#define SIZE_MTD 3

unsigned long cpt_ne = 0;

int *cons;
int *var;

double *val;

int ne = 1;

//              pf  n_d  md n_a  ma
double Rpnmnm [SIZE][SIZE][SIZE_MTD][SIZE][SIZE_MTD];
double Cpnmnm [SIZE][SIZE][SIZE_MTD][SIZE][SIZE_MTD];
double Rhpnmnm[SIZE][SIZE][SIZE_MTD][SIZE][SIZE_MTD];
double Chpnmnm[SIZE][SIZE][SIZE_MTD][SIZE][SIZE_MTD];


unsigned long var_number = 0;

int row_znnmdp = 0;
int one_row = 0;

double profile_proba[10000];

/* Create the constraints names  */
void constraints_name_creator (char *name , int cons_n, int node_n, int mtd_n, int profil_n, int ids) {
  int cpt = 0;

  name[cpt++] = 'c';
  name[cpt++] = cons_n/10 + '0';
  name[cpt++] = cons_n%10 + '0';

  if (profil_n >= 0) {
    name[cpt++] = 'p';
    name[cpt++] = (profil_n/10000) % 10 + '0';
    name[cpt++] = (profil_n/1000) % 10 + '0';
    name[cpt++] = (profil_n/100) % 10 + '0';
    name[cpt++] = (profil_n/10) % 10 + '0';
    name[cpt++] = profil_n%10 + '0';    
  }

  if (node_n >= 0) {
    name[cpt++] = 'n';
    name[cpt++] = (node_n/10000) % 10 + '0';
    name[cpt++] = (node_n/1000) % 10 + '0';
    name[cpt++] = (node_n/100) % 10 + '0';
    name[cpt++] = (node_n/10) % 10 + '0';
    name[cpt++] = node_n%10 + '0';
  }

  if (mtd_n >= 0) {  
    name[cpt++] = 'm';
    name[cpt++] = (mtd_n/10000) % 10 + '0';
    name[cpt++] = (mtd_n/1000) % 10 + '0';
    name[cpt++] = (mtd_n/100) % 10 + '0';
    name[cpt++] = (mtd_n/10) % 10 + '0';
    name[cpt++] = mtd_n%10 + '0';
  }

  if (ids > 0) {
    name[cpt++] = 'd';
  }
  
  name[cpt] = '\0';
  return;
}

void create_constraints (glp_prob *mip, unsigned long *cpt, int node_n, int mtd_n, int profil_n, int ids, double lb, double ub, int cons_type, int cons_nb, int* cons_number) {
  char name[5000];

  cons_number[0] = glp_add_rows(mip, 1);
  constraints_name_creator(name, cons_nb, node_n, mtd_n, profil_n, ids);
  glp_set_row_name (mip, cpt[0] , name);
  glp_set_row_bnds (mip, cpt[0], cons_type, lb, ub);
  cpt[0]++;

#ifdef VERBOSE
  printf("%s\n", name);
#endif 
  
  return; 
}


/* Add all the constraints to the problem */
void add_constraints (glp_prob *mip, int *cons_numbers, int profil_number, int node_number, double  M, int row_zij, int mtd_max, int *mtd_node_number) {
  unsigned long cpt = 1;

#ifdef VERBOSE
  printf("%d\n", cons_numbers[0]);
#endif 
  
  
  //glp_add_rows(mip, cons_numbers)
  for (int n = 0 ; n < node_number ; n ++) {

    // cons 1 : 
    create_constraints (mip, &cpt, n, -1, -1, -1, 1.0, 1.0, GLP_FX, 1, cons_numbers);
    
    // cons 2 : 
    for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
      create_constraints (mip, &cpt, n, mtd , -1, -1, 0.0, 1.0, GLP_UP, 2, cons_numbers);
    }

    // cons 5 L :
    for (int m = 0 ; m < profil_number ; m++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	create_constraints (mip, &cpt, n, mtd , m, -1, 0.0, 0.0, GLP_LO, 5, cons_numbers);
      }
    }

    // cons 5 R :
    for (int m = 0 ; m < profil_number ; m++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	create_constraints (mip, &cpt, n, mtd , m, -1, 0.0, M, GLP_UP, 6, cons_numbers);
      }
    }    
  }

  for (int m = 0 ; m < profil_number ; m++) {
    // cons 3 :
    for (int n = 0 ; n < node_number ; n++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	create_constraints (mip, &cpt, n, mtd , m, -1, 0.0, 0.0, GLP_FX, 3, cons_numbers);
      }
    }


    // cons 4 :
    create_constraints (mip, &cpt, -1, -1 , m, -1, 1.0, 1.0, GLP_FX, 4, cons_numbers);

  }
    
#ifdef VERBOSE
    printf("%ld\n", cpt);
#endif 
#ifdef VERBOSE
    printf("%d\n", cons_numbers[0]);
#endif 

  return;
  }




void Z_name_creator (char *name , int node_d, int mtd_d, int profil_p, int node_a, int mtd_a, int def_ids, int att_ids) {
  int cpt = 0;

  name[cpt++] = 'Z';

  //node number defender
  name[cpt++] = 'n';
  name[cpt++] = (node_d / 10000) % 10  + '0';
  name[cpt++] = (node_d / 1000) % 10  + '0';
  name[cpt++] = (node_d / 100) % 10  + '0';
  name[cpt++] = (node_d / 10) % 10 + '0';
  name[cpt++] = node_d % 10  + '0';
  
  if (def_ids == 1) {
    //IDS On defender node
    name[cpt++] = 'd';
  }
  else {
    // MTD number defender
    name[cpt++] = 'm';
    name[cpt++] = (mtd_d / 10000) % 10  + '0';
    name[cpt++] = (mtd_d / 1000) % 10  + '0';
    name[cpt++] = (mtd_d / 100) % 10  + '0';
    name[cpt++] = (mtd_d / 10) % 10 + '0';
    name[cpt++] = mtd_d % 10  + '0';    
  }

  //profil number
  name[cpt++] = 'p';
  name[cpt++] = (profil_p/ 10000) % 10  + '0';
  name[cpt++] = (profil_p/ 1000) % 10  + '0';
  name[cpt++] = (profil_p/ 100) % 10  + '0';
  name[cpt++] = (profil_p/ 10) % 10 + '0';
  name[cpt++] = profil_p%10  + '0';

  /* ----------- */
  
  //node number attacker
  name[cpt++] = 'n';
  name[cpt++] = (node_a / 10000) % 10  + '0';
  name[cpt++] = (node_a / 1000) % 10  + '0';
  name[cpt++] = (node_a / 100) % 10  + '0';
  name[cpt++] = (node_a / 10) % 10 + '0';
  name[cpt++] = node_a % 10  + '0';
  
  if (att_ids == 1) {
    //IDS On attacker node
    name[cpt++] = 'd';
  }
  else {
    // MTD number attacker
    name[cpt++] = 'm';
    name[cpt++] = (mtd_a / 10000) % 10  + '0';
    name[cpt++] = (mtd_a / 1000) % 10  + '0';
    name[cpt++] = (mtd_a / 100) % 10  + '0';
    name[cpt++] = (mtd_a / 10) % 10 + '0';
    name[cpt++] = mtd_a % 10  + '0';    
  }

  name[cpt++] = '\0';

#ifdef VERBOSE
  printf("node %d mtd %d profil %d node %d mtd %d ids def %d ids att %d - name : %s\n",node_d, mtd_d, profil_p, node_a, mtd_a, def_ids, att_ids, name );
#endif
  
}


void create_Znmpnm(glp_prob *mip, int node_number, int *mtd_node_number, unsigned long *cpt, int m) {
  char name[5000];

#ifdef VERBOSE
  printf("in Znmpnm\n");
#endif

  for (int node_d = 0 ; node_d < node_number ; node_d++) {
    for (int mtd_d = 0 ; mtd_d < mtd_node_number[node_d] ; mtd_d++) {
      for (int node_a = 0 ; node_a < node_number ; node_a++) {
	for (int mtd_a = 0 ; mtd_a < mtd_node_number[node_a] ; mtd_a++) {

	  Z_name_creator (name , node_d, mtd_d, m, node_a, mtd_a, 0, 0);     
	  glp_add_cols(mip, 1);
	  var_number++;
	  glp_set_col_name(mip, cpt[0], name);
	  glp_set_col_bnds(mip, cpt[0], GLP_DB, 0, 1);
	  glp_set_obj_coef(mip, cpt[0],  (profile_proba[m] * Rpnmnm[m][node_d][mtd_d][node_a][mtd_a]) - Cpnmnm[m][node_d][mtd_d][node_a][mtd_a]);	  
	  glp_set_col_kind (mip, cpt[0], GLP_CV);
	  cpt[0]++;
#ifdef VERBOSE
	  printf("%s\n", name);
#endif 	
	}	
      }
    }
  }
  return;
}


void Id_name_creator (char *name , int node_d) {
  int cpt = 0;

  name[cpt++] = 'I';
  name[cpt++] = 'd';
  name[cpt++] = 'l';
  name[cpt++] = 'e';
  name[cpt++] = '_';
  
  //node number defender
  name[cpt++] = 'n';
  name[cpt++] = (node_d / 10000) % 10  + '0';
  name[cpt++] = (node_d / 1000) % 10  + '0';
  name[cpt++] = (node_d / 100) % 10  + '0';
  name[cpt++] = (node_d / 10) % 10 + '0';
  name[cpt++] = node_d % 10  + '0';
  
  name[cpt++] = '\0';

#ifdef VERBOSE
  printf("node %d - name : %s\n",node_d, name );
#endif
  
}


void create_Idle(glp_prob *mip, int node_number, unsigned long *cpt) {
  char name[5000];

#ifdef VERBOSE
  printf("in IDLE\n");
#endif

  for (int node_d = 0 ; node_d < node_number ; node_d++) {

	  Id_name_creator (name , node_d);     
	  glp_add_cols(mip, 1);
	  var_number++;
	  glp_set_col_name(mip, cpt[0], name);
	  glp_set_col_bnds(mip, cpt[0], GLP_DB, 0, 1);
	  glp_set_obj_coef(mip, cpt[0], 0);	  
	  glp_set_col_kind (mip, cpt[0], GLP_CV);
	  cpt[0]++;
#ifdef VERBOSE
	  printf("%s\n", name);
#endif 	
  }
  return;
}



void alpha_name_creator (char *name , int profil_n, int node_n, int mtd_n, int ids) {
  int cpt = 0;
  
  name[cpt++] = 'a';
  name[cpt++] = 'l';
  name[cpt++] = 'p';
  name[cpt++] = 'h';
  name[cpt++] = 'a';
  name[cpt++] = '_';
  //profile number
  name[cpt++] = 'p';
  name[cpt++] = (profil_n / 10000) % 10  + '0';
  name[cpt++] = (profil_n / 1000) % 10  + '0';
  name[cpt++] = (profil_n / 100) % 10  + '0';
  name[cpt++] = (profil_n / 10) % 10 + '0';
  name[cpt++] = profil_n %10  + '0';
  //node number
  name[cpt++] = 'n';
  name[cpt++] = (node_n / 10000) % 10  + '0';
  name[cpt++] = (node_n / 1000) % 10  + '0';
  name[cpt++] = (node_n / 100) % 10  + '0';
  name[cpt++] = (node_n / 10) % 10 + '0';
  name[cpt++] = node_n % 10  + '0';

  if (ids == 1) {
    name[cpt++] = 'd';
  }
  else {
    // mtd number
    name[cpt++] = 'm';
    name[cpt++] = (mtd_n / 10000) % 10  + '0';
    name[cpt++] = (mtd_n / 1000) % 10  + '0';
    name[cpt++] = (mtd_n / 100) % 10  + '0';
    name[cpt++] = (mtd_n / 10) % 10 + '0';
    name[cpt++] = mtd_n % 10  + '0';
  }
  
  name[cpt] ='\0';

#ifdef VERBOSE
  printf("profil %d node %d mtd %d ids def %d - name : %s\n", profil_n, node_n, mtd_n, ids, name );
#endif

  
  return;
}


void create_alpha (glp_prob *mip, int node_number, int *mtd_node_number, unsigned long *cpt, int m) {
  char name[50000];

  // alpha bound : true
  // lb = 0 up = 1
  // Binary / integer
  // obj func coef = 0
  for (int node_n = 0 ; node_n < node_number ; node_n++) {
    for (int mtd_n = 0 ; mtd_n < mtd_node_number[node_n] ; mtd_n++) {
      alpha_name_creator (name, m, node_n, mtd_n, 0);

      glp_add_cols(mip, 1);
      var_number++;

      glp_set_col_name(mip, cpt[0], name);
      glp_set_col_bnds(mip, cpt[0], GLP_DB, 0.0, 1.0);
      glp_set_obj_coef(mip, cpt[0], 0.0);
      glp_set_col_kind (mip, cpt[0], GLP_BV);
      cpt[0]++;
#ifdef VERBOSE
      printf("%s\n", name);
#endif 
    }
  }
  return;
}


void A_name_creator (char *name , int m) {

  name[0] = 'A';
  if (m < 10) {
    name[1] = '_';
    name[2] = 'p';
    name[3] = m + '0';
    name[4] = '\0';    
  }
  else if (m < 100) {
    name[1] = '_';
    name[2] = 'p';
    name[3] = m/10 + '0';
    name[4] = m%10 + '0';
    name[5] = '\0';    
  }
  else if (m < 1000) {
    name[1] = '_';
    name[2] = 'p';
    name[3] = m/100 + '0';
    name[4] = (m/10)%10 + '0';
    name[5] = m%10 + '0';
    name[6] = '\0';    
  }

#ifdef VERBOSE
  printf("profil %d name : %s\n", m, name );
#endif

  
  return;
}

void create_A (glp_prob *mip, int node_number, unsigned long *cpt, int m) {
  char name[10000];

  // A bound : false
  // Continuous
  // obj func coef = 0
  A_name_creator (name ,  m);

  glp_add_cols(mip, 1);
  var_number++;  

  glp_set_col_name(mip, cpt[0], name);
  glp_set_col_bnds(mip, cpt[0], GLP_FR, 0, 0);
  glp_set_obj_coef(mip, cpt[0], 0.0);
  glp_set_col_kind (mip, cpt[0], GLP_CV);
  cpt[0]++;
#ifdef VERBOSE
      printf("%s\n", name);
#endif 
  return;
}  

void add_variables(glp_prob *mip, int profil_number, int node_number, int *mtd_node_number ) {

  unsigned long cpt = 1;
  double k = node_number;
  
  for (int m = 0 ; m < profil_number ; m++) {
    create_Znmpnm(mip, node_number, mtd_node_number, &cpt, m);
    if (m == 0) {
      row_znnmdp = var_number;
    }
    create_alpha (mip, node_number, mtd_node_number, &cpt, m);
    create_A (mip, node_number, &cpt, m);
    if (m == 0) {
      one_row = var_number;
    }    
  }

  create_Idle(mip, node_number, &cpt);



#ifdef VERBOSE
      printf("var number count %ld\n", cpt);
#endif 

  return;
}































/*--------- Constraints settings  ----------*/

void set_cons_Zn (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int n, int* mtd_node_number, int node_number) {

  int shift = 0;
  int nb = 0;

  for (int k = 0 ; k < mtd_node_number[n]; k++) {
    for (int j = 0 ; j < node_number; j++) {
      nb += (mtd_node_number[j]);
    }
  }

  for (int y = 0 ; y < n; y++) {
    for (int k = 0 ; k < mtd_node_number[y]; k++) {
      for (int j = 0 ; j < node_number; j++) {
	shift += (mtd_node_number[j]);
      }
    }
  }
  
  for (unsigned long o = 0 ; o < profil_number ; o++) {
    
    for (int i = shift; i < shift + nb ; i++ ) {
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1 ;
      val[ne] = 1;
      ne ++;
    }
    shift += one_row;
  }

  /* set the Idle  */
  cons[ne] = consnb[0];
  var[ne] = (var_numbers - node_number) + n +1 ;
  val[ne] = 1;
  ne ++;
  
  
  consnb[0] ++;  
  return;
}


void set_cons_delta_m (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int n, int* mtd_node_number, int node_number, int m) {

  int shift = 0;
  int nb = 1;

  for (int j = 0 ; j < node_number; j++) {
    //nb += (mtd_node_number[j]);
  }

  for (int y = 0 ; y < n; y++) {
    for (int k = 0 ; k < mtd_node_number[y]; k++) {
      for (int j = 0 ; j < node_number; j++) {
	shift += (mtd_node_number[j]);
      }
    }
  }

  int dec_mtd = 0;

  for (int j = 0 ; j < node_number; j++) {
    dec_mtd += (mtd_node_number[j]);
  }


  
  for (int i = 0 ; i < m ; i++) {
    shift += dec_mtd;
  }
  

  for (unsigned long o = 0 ; o < profil_number ; o++) {
    
    for (int i = shift; i < shift + dec_mtd ; i++ ) {
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1 ;
      val[ne] = 1;
      ne ++;
    }
    shift += one_row;
  }
    
  consnb[0] ++;  
  return;

  
}


void set_cons_3  (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int p, int* mtd_node_number, int node_n, int mtd_n, int node_number) {

  /*------------ Znmpnm -------------*/
  
  int shift = 0;
  int nb = 0;

  // décalage profile
  shift = p * one_row; 

  // decalage noeud
  for (int i = 0 ; i < node_n ; i++) {
    shift += mtd_node_number[i];
  }

  // decalage mtd
  shift += mtd_n;
  
  for (int i = 0 ; i < node_number ; i++) {
    for (int j = 0 ; j < mtd_node_number[i] ; j++) {
      
      cons[ne] = consnb[0];
      var[ne] = shift%var_numbers +1;//i%var_numbers + 1 + one_row*m;
      val[ne] = 1;
      ne ++;
      
      for (int j = 0 ; j < node_number ; j++) {
	shift += mtd_node_number[j];
      }
    }

  }

  /*------------ alpha -------------*/
  
  shift = 0;
  shift = p * one_row;
  shift += row_znnmdp;

  for (int i = 0 ; i < node_n ; i++ ) {
    shift += mtd_node_number[i];
  }

  shift += mtd_n;  
  cons[ne] = consnb[0];
  var[ne] = shift%var_numbers +1;//i%var_numbers + 1 + one_row*m;
  val[ne] = -1;
  ne ++;
    
  consnb[0] ++;  
  return;
}


void set_cons_Alpha (unsigned long *cpt, int *consnb, unsigned long var_numbers,int node_number, int profil_number, int one_row, int row_zij, int m, int * mtd_node_number) {

  int max = 0;
  for (int n = 0 ; n < node_number ; n++) {
    // for the mtd
    max += mtd_node_number[n];
  }
  
  // alpha
  for (unsigned long i = (var_numbers * m) + row_zij ; i < (var_numbers * m) + row_zij + max  ; i++) {
    cons[ne] = consnb[0];
    var[ne] = i%var_numbers + 1 + one_row*m;
    val[ne] = 1;
    ne ++;
  }

  cpt[0] += (var_numbers * profil_number);
  consnb[0] ++;
  return;
}

void set_cons_6r (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int p, int* mtd_node_number, int node_n, int mtd_n, int node_number, double M) {

  if (mtd_node_number[node_n] < mtd_n) {
    return;
  }
  
  int shift = 0;
  
  // Set the Z
  shift = p * one_row;

  // n
  for (int i = 0 ; i < node_number ; i++) {
    // m
    for (int j = 0 ; j < mtd_node_number[i] ; j++) {
      // n'
      for (int l = 0 ; l < node_number ; l++) {
	// m'
	for (int q = 0 ; q < mtd_node_number[l] ; q++) {
	  cons[ne] = consnb[0];
	  var[ne] = shift%var_numbers + 1 ;
	  val[ne] = - Rhpnmnm[p][i][j][node_n][mtd_n];
	  ne ++;
	  shift++;
	}
      }
    }
  }

  //set the alpha
  
  shift = row_znnmdp;

  for (int i = 0 ; i < node_n ; i++) {
    shift += mtd_node_number[i];
  }

  shift += mtd_n;

  cons[ne] = consnb[0];
  var[ne] = shift+1 + one_row*p ;
  val[ne] = M;
  ne ++;
  
  // set the ap;
  cons[ne] = consnb[0];
  var[ne] = one_row*(p+1);;
  val[ne] = 1;
  ne ++;

  consnb[0] ++;  
  return;
}



void set_cons_6l (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int p, int* mtd_node_number, int node_n, int mtd_n, int node_number, double M) {

  if (mtd_node_number[node_n] < mtd_n) {
    return;
  }
  
  int shift = 0;

  // Set the Z
  shift = p * one_row;

  // n
  for (int i = 0 ; i < node_number ; i++) {
    // m
    for (int j = 0 ; j < mtd_node_number[i] ; j++) {
      // n'
      for (int l = 0 ; l < node_number ; l++) {
	// m'
	for (int q = 0 ; q < mtd_node_number[l] ; q++) {
	  cons[ne] = consnb[0];
	  var[ne] = shift%var_numbers + 1 ;
	  val[ne] = - Rhpnmnm[p][i][j][node_n][mtd_n];
	  ne ++;
	  shift++;
	}
      }
    }
  }
  
  // set the ap;
  cons[ne] = consnb[0];
  var[ne] = one_row*(p+1);;
  val[ne] = 1;
  ne ++;

  consnb[0] ++;  
  return;
}



/*--------- Constraints configuration  ----------*/


void set_constraints (glp_prob *mip, unsigned long var_numbers, int node_number, int *mtd_node_number, int profil_number, int one_row, int row_zij, double k, double M, int cons_numbers, int mtd_max) {
  
  long int last = 0;
  unsigned long cpt = 1;
  int consnb = 1;
  int t = 0;

  long int min = 0, max = 0;


  printf ("set constraint 1\n");
  for (int n = 0 ; n < node_number ; n++) {
    set_cons_Zn (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, n, mtd_node_number, node_number);
    
    min = 0;
    max = 0;
    
    // constraints 2 :
    for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
      set_cons_delta_m (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, n, mtd_node_number, node_number, mtd); 
    }
    
    // constraints 5l :
    for (int m = 0 ; m < profil_number ; m++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	set_cons_6l (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, n, mtd, node_number, M);
      }
    }

    // constraints 5r :
    for (int m = 0 ; m < profil_number ; m++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	set_cons_6r (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, n, mtd, node_number, M);
      }
    }
  }

  for (int m = 0 ; m < profil_number ; m++) {

    // constraints 3  :
    for (int n = 0 ; n < node_number ; n++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	set_cons_3 (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, n, mtd, node_number );
      }
    }

    
    // constraints 4 :
    set_cons_Alpha (&cpt, &consnb, var_numbers, node_number, profil_number, one_row, row_zij, m, mtd_node_number );
  }
  
  printf ("cpt : %ld, matrix \n", cpt);
  printf ("ne : %d, element != 0 \n", ne);  
  glp_load_matrix(mip, ne-1, cons, var, val);
  printf ("cpt = %ld \n", cpt  );  
  return;
}


void fill_constant(FILE *fp, int *node_number, int mtd_node_number[] ,int *profil_number, double *M, char pb_name[255], double profil_proba[1000]) {

  char buff[455];
  unsigned long cpt = 0;
  int o;

  // Fetch the problem name
  while (fgets(buff, 255, fp) != NULL) {
    // '#' = commentary in the configuration file
    if (buff[0] != '#') {
      strcpy (pb_name, buff);
      pb_name[strlen(pb_name)-1] = '\0';
      break;
    }
  }
  
  // Fill the constants
  while (fscanf(fp, "%s", buff) != EOF) {
    // '#' = commentary in the configuration file
    if (buff[0] != '#') {
      if (cpt == 0) {
	*profil_number = atof(buff);
	cpt ++;
#ifdef VERBOSE
	printf ("profil_number : %d \n", *profil_number);
#endif
      }
      else if (cpt == 1) {
      	*node_number = atof(buff);
      	cpt ++;
#ifdef VERBOSE
	printf ("node_number : %d \n", *node_number);
#endif
      }
      else if (cpt < 2 + *node_number) {
      	mtd_node_number[cpt-2] = atof(buff);
      	cpt ++;
#ifdef VERBOSE
      	printf ("mtd_node_number : %d \n", mtd_node_number[cpt-3]);
#endif
      }
      else if (cpt == 2 + *node_number) {
      	*M = atof(buff);
#ifdef VERBOSE
      	printf ("M : %f \n", *M);
#endif
      	cpt++;
      	o=0;
      }
      else if (cpt > 2 + *node_number) {
	profil_proba[o] = atof(buff);
#ifdef VERBOSE
      	printf ("profil_proba[%d] : %f \n", o, profil_proba[o]);
#endif
      	o++;
      	if (o == *profil_number){
      	  break;
	}
      }
    }
    else {
      fgets(buff, 255, fp); 
    }
  }
  return;
}

void fill_reward_rnmpnm (FILE *fp, int *mtd_node_number, int node_number, int profil_number, double R[SIZE][SIZE][SIZE_MTD][SIZE][SIZE_MTD],char*name) {
  char buff[255];
  unsigned long cpt = 0;
  int profil_n, node_d, mtd_d, node_a, mtd_a;

  // Fill the Rijs tab
  while (fscanf(fp, "%s", buff) != EOF) {
    // '#' = commentary in the configuration file    
    if (buff[0] != '#') {
      if (strcmp (buff, "STOP") == 0) {
      	return;
      }      
      if (cpt == 0) {
      	profil_n = atof (buff);
      	cpt ++;
      }
      else if (cpt == 1) {
      	node_d = atof (buff);
      	cpt ++;
      }
      else if (cpt == 2) {
      	mtd_d = atof (buff);
      	cpt ++;
      }
      else if (cpt == 3) {
      	node_a = atof (buff);
      	cpt ++;
      }
      else if (cpt == 4) {
      	mtd_a = atof (buff);
      	cpt ++;
      }

      else {
	R[profil_n][node_d][mtd_d][node_a][mtd_a] = atof(buff);
#ifdef VERBOSE
      	printf ("%s[%d][%d][%d][%d][%d] = %f \n", name, profil_n, node_d, mtd_d, node_a, mtd_a, R[profil_n][node_d][mtd_d][node_a][mtd_a]);
#endif
	cpt = 0;
      }
    }
    else {
      fgets(buff, 255, fp); 
    }
} 
  return;
}




void fill_matrices(FILE *fp, int node_number, int mtd_node_number[],int profil_number, double M ) {

  printf ("\n");

  //For the Defender
  fill_reward_rnmpnm (fp, mtd_node_number, node_number, profil_number, Rpnmnm, "Rpnmnm");
  fill_reward_rnmpnm (fp, mtd_node_number, node_number, profil_number, Cpnmnm, "Cpnmnm");

  //For the Attacker
  fill_reward_rnmpnm (fp, mtd_node_number, node_number, profil_number, Rhpnmnm, "Rhpnmnm");
  fill_reward_rnmpnm (fp, mtd_node_number, node_number, profil_number, Chpnmnm, "Chpnmnm");

  printf ("\n"); 
  return;
}


int main(int argc, char* argv[])
{  
  if (argc != 3) { 
    printf ("Wrong usage, 1 arg required :\n ./solver.x \"configuration file\" \"g\" for GLPK usage or \"c\" for CLEPX usage \n");  
    return -1; 
  } 

  FILE *fp; 
  int node_number; 
  int mtd_node_number[550];
  int mtd_max;
  int profil_number;
  double M;
  char pb_name[255];



  val  = malloc(1000000000 *sizeof(double));
  cons = malloc(1000000000 *sizeof(double));
  var  = malloc(1000000000 *sizeof(double));

  fp = fopen(argv[1], "r"); 
  if (fp == NULL) { 
    printf ("file %s doesn'n exist \n", argv[1]);  
    return -1;     
  } 

  
  fill_constant(fp, &node_number, mtd_node_number, &profil_number, &M, pb_name, profile_proba);  
  for (int i = 0 ; i < node_number ; i++ ) {
    mtd_max +=  mtd_node_number[i];
  }
  fill_matrices(fp, node_number, mtd_node_number, profil_number, M);

  double k = node_number;
      
  int cons_numbers =  0; 
  
  /* Create the Problem with the name extract from the file 
     As a Maximization problem
   */
  glp_prob *mip = glp_create_prob();
  glp_set_prob_name (mip, pb_name);
  glp_set_obj_dir (mip, GLP_MAX);

  
  /* Add the constraints to the problem  */
  add_constraints (mip, &cons_numbers, profil_number, node_number, M, row_znnmdp, mtd_max, mtd_node_number);
  
  /* Add the variables to the problem  */
  add_variables(mip, profil_number, node_number, mtd_node_number);

#ifdef VERBOSE
  printf("var_number : %ld, one_row : %d, row_znnmdp %d \n", var_number, one_row, row_znnmdp);
#endif 

  
  /* Add the constraints to the problem  */
  set_constraints (mip, var_number, node_number, mtd_node_number, profil_number, one_row, row_znnmdp, k,  M, cons_numbers, mtd_max);
  
#ifdef VERBOSE
  printf ("**Problem name : %s **\n\n", glp_get_prob_name(mip));  
  printf ("**The objective function direction (1:MIN;2:MAX) : %d **\n", glp_get_obj_dir(mip));
  printf ("\nThe constraints : \n");
  for (int i = 1 ; i <= cons_numbers ; i++) {
     printf("%s lb : %g; ub : %g\n", glp_get_row_name(mip, i),  glp_get_row_lb(mip, i), glp_get_row_ub(mip, i));
   }
#endif

  int ind[10000];
  double val[10000];
  
  int len;
  int c = 0;

#ifdef VERBOSE
  printf ("\n**The constraints coeffs values: **\n");
  for (int i = 1 ; i <= cons_numbers ; i++) {

    printf("%s : ", glp_get_row_name(mip, i));
    len = glp_get_mat_row(mip, i, ind, val);
   
    for (int j = 1 ; j <= var_number; j++) {
      c = 0;
      for (int l = 1 ; l <= len ; l ++) {
	if (ind[l] == j) {
	  printf("%g*%s + ", val[l], glp_get_col_name(mip, j));
	  c = 1;
	}
      }
      if (c == 0 ) {
	printf("%g*%s + ", 0.0, glp_get_col_name(mip, j));
      }
      if (j%one_row == 0) {
	printf("\n       ");
      }
      else if (j%node_number == 0) {
	printf("\n       ");
      }
    }
    printf("\n");
  }
  printf("\n");



     printf ("\n**The objective function definition : **\n");
   for (int i = 1 ; i <= var_number ; i++) {
     printf("%g*%s + ", glp_get_obj_coef(mip, i),  glp_get_col_name(mip, i));
     if ((i % one_row) == 0) {
       printf("\n");
     }
     if ((i % node_number) == 0) {
       printf("\n");
     }
   }
   printf("\n");
#endif  
  
  /* Solve the problem  */
  glp_iocp parm;
  glp_init_iocp(&parm);
  parm.presolve = GLP_ON;

  // Check if using CPLEX and return
  if (argv[2][0] == 'c') {
    char result_file[300];
    sprintf (result_file, "./test.lp");//, argv[1]);
    glp_write_lp(mip, NULL, result_file);
    return 0;
  }

  // Or using GLPK and continue
  glp_intopt(mip, &parm);
  printf("\n** The result**\n");

  double output[2000];
  for (int i = 0 ; i < one_row ; i++) {

    output[i] = 0;


  }

  
  int g = 0;
  int pf = 0;
  int count = 0;
  int count_n = 0;
  int sov_g = 0;
  
/*   for (int i = 1 ; i <= profil_number; i++) { */
/*     for (int j = 1 ; j <= one_row ; j++) { */
/*       if (j%node_number == 0) { */
/* 	printf (" %s = %g\n", glp_get_col_name(mip, (((i-1)*one_row)+j)), (glp_mip_col_val(mip, (((i-1)*one_row)+j)))); */
/*       } */
/*       else { */
/* 	printf (" %s = %g -  ", glp_get_col_name(mip, (((i-1)*one_row)+j)), (glp_mip_col_val(mip, (((i-1)*one_row)+j)))); */
/*       } */
/*     } */
/*   } */

/*   for (int i = (var_number-node_number) +1; i < var_number+1 ; i ++) { */
/*     printf("\n %s = %.2f - ", glp_get_col_name(mip, i), glp_mip_col_val(mip,i)); */
/*   } */
/*   printf("\n\n"); */

/*   unsigned long cptt = 0; */
/*   unsigned long cptt2 = 0; */
/*   int max = 0; */


/* for (int i = 1 ; i <= var_number; i++) { */
/*   printf ("%s = %lf \n", glp_get_col_name(mip, i), glp_mip_col_val(mip,i)); */
  
/*  } */

 float delta[node_number][50];
 float alpha[profil_number][node_number][50];

 unsigned long counter = 0;


 for (int t = 0 ; t < profil_number ; t++) {
   for (int i = 0 ; i < node_number ; i++) {
     for (int j = 0 ; j < mtd_node_number[i] ; j++) {
       delta[i][j] = 0.0;
        alpha[t][i][j] = 0.0;

     }
   }
 }
 
 
 for (int t = 0 ; t < profil_number ; t++) {
   
   //extract delta
   for (int i = 0 ; i < node_number ; i++) {
     for (int j = 0 ; j < mtd_node_number[i] ; j++) {
  
       for (int k = 0 ; k < node_number ; k++) {
	 for (int l = 0 ; l < mtd_node_number[k] ; l++) {
	   counter++;
	   delta[i][j] += glp_mip_col_val(mip,counter);
	 }
       }
       //       printf("\\delta_{n%dm%d} = %lf\n", i, j ,delta[i][j]);
     }
   }

   //extract alpha
   for (int i = 0 ; i < node_number ; i++) {
     for (int j = 0 ; j < mtd_node_number[i] ; j++) {
       counter ++;
       printf("\\alpha_{p%dn'%dm'%d} = %lf\n", t, i, j , glp_mip_col_val(mip,counter));
     }
   }

   counter ++;

 }

 //print delta
 for (int i = 0 ; i < node_number ; i++) {
   for (int j = 0 ; j < mtd_node_number[i] ; j++) {
     printf("\\delta_{n%dm%d} = %lf\n", i, j , delta[i][j]);
   }
   counter ++;
   printf("\\delta_{n%didl} = %lf\n", i, glp_mip_col_val(mip,counter));
 }


 
  /* for (int i = 0 ; i < node_number ; i++) { */
  /*   for (int j = 0 ; j < mtd_node_number[i] ; j++) { */
  /*     printf ("\\delta_{%d%d} =  %.4f - ", i+1, j+1,  output[cptt]); */
  /*     cptt++; */
  /*   } */
  /* } */

  /* int t = 1; */
  /* for (int i = 1 ; i <= var_number; i++) { */

  /*   if ((i % one_row) > row_znnmdp) { */
  /*     printf("\\alpha_{%d%d} = %.2f - ", t, (i%one_row)-row_znnmdp, glp_mip_col_val(mip,i)); */
  /*   } */
  /*   if ((i % one_row) == 0) { */
  /*     t++; */
  /*     printf ("\n"); */
  /*   } */
  /* } */
  
  printf("\n");


   return 0;
}

