#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define LENGTH 1000

// Start to fill the defender parameters
double Wp[LENGTH][LENGTH];
double Cqos[LENGTH][LENGTH];

double Sp[LENGTH][LENGTH][3];
double Whp[LENGTH][LENGTH];
double Chp[LENGTH][LENGTH];

int *mtd_node_number;
double *attacker_proba;

void fill_param_2d (FILE *fp_in, char* buff, int nb_profiles, int node_number, double param[LENGTH][LENGTH]) {

  int n = 0, p = 0;
  double val = 0.;
    
  while (strcmp (buff, "STOP") != 0) {
    p = atoi(buff);
    //    printf("p : %d ", p);
    fscanf(fp_in, "%s", buff);
    n = atoi(buff);
    //printf("n : %d ", n);
    fscanf(fp_in, "%s", buff);
    val = atof(buff);
    //printf("val : %g ", val);
    param[p][n]=val;
    //printf("param : %g\n", param[p][n]);
    fscanf(fp_in, "%s", buff);
  }
}

void fill_param_3d (FILE *fp_in, char* buff, int nb_profiles, int node_number, double param[LENGTH][LENGTH][3]) {

  int n = 0, p = 0, m = 0;
  double val = 0.;
  
  while (strcmp (buff, "STOP") != 0) {
    p = atoi(buff);
    //printf("p : %d ", p);
    fscanf(fp_in, "%s", buff);
    n = atoi(buff);
    //printf("n : %d ", n);
    fscanf(fp_in, "%s", buff);
    m = atoi(buff);
    //printf("m : %d ", m);
    fscanf(fp_in, "%s", buff);
    val = atof(buff);
    //printf("val : %g ", val);
    param[p][n][m]=val;
    //printf("param : %g\n", param[p][n][m]);
    fscanf(fp_in, "%s", buff);
  }
}

void fill_param_1d (FILE *fp_in, char* buff, int node_number, double param[node_number]) {

  int n = 0;
  double val = 0.;
  
  while (strcmp (buff, "STOP") != 0) {
    n = atoi(buff);
    fscanf(fp_in, "%s", buff);
    val = atof(buff);
    param[n]=val;
    fscanf(fp_in, "%s", buff);
  }
}



void escape_comment(FILE *fp_in, FILE *fp_out, char*buff) {
  fscanf(fp_in, "%s", buff);
   while (buff[0] == '#') {
     fprintf (fp_out, "%s", buff);
     fgets(buff, 555, fp_in);
     fprintf (fp_out, "%s", buff);
     fscanf(fp_in, "%s", buff);
   }  
  return ;
}


int main(int argc, char* argv[])
{  
   if (argc != 3) { 
     printf ("Wrong usage, 2 arg required :\n ./parameters_to_rewards_and_costs.x \"input_file\"  \"output_file\" \n");  
     return -1; 
   } 

   // Input file
   FILE *fp_in; 
   fp_in = fopen(argv[1], "r"); 
   if (fp_in == NULL) { 
     printf ("error opening input file %s \n", argv[1]);  
     return -1;     
   }

   // Output file
   FILE *fp_out; 
   fp_out = fopen(argv[2], "w"); 
   if (fp_out == NULL) { 
     printf ("error opening output file %s \n", argv[2]);  
     return -1;     
   }
   
   // Get the Game parameters value
   int nb_profiles = 0;
   int node_number = 0;
   char buff[556];

   escape_comment(fp_in, fp_out, buff);
   //get the problem name
   fprintf (fp_out, "%s", buff);
   fgets(buff, 255, fp_in);
   fprintf(fp_out, "%s", buff);	
   
   escape_comment(fp_in, fp_out, buff);
   // get the nimber of attacker profile 
   nb_profiles = atoi(buff);
   fprintf (fp_out, "%s\n", buff);
   
   escape_comment(fp_in, fp_out, buff);
   // get the number of node 
   node_number = atoi(buff);
   fprintf (fp_out, "%s\n", buff);

   escape_comment(fp_in, fp_out, buff);
   // get the number of MTD per node
   mtd_node_number = malloc(node_number*sizeof(int)*2);  
   for (int i = 0 ; i < node_number ; i++) {
     mtd_node_number[i] = atoi(buff);
     fprintf (fp_out, "%s\n", buff);
     escape_comment(fp_in, fp_out, buff);
   }

   // get the M value 
   fprintf (fp_out, "%s\n", buff);

   escape_comment(fp_in, fp_out, buff);
   // the attacker proba for each profiles
   attacker_proba = malloc(nb_profiles*sizeof(int)*2);  
   for (int i = 0 ; i < nb_profiles ; i++) {
     attacker_proba[i] = atof(buff);
     fprintf (fp_out, "%s\n", buff);
     escape_comment(fp_in, fp_out, buff);
   }
   
   int mtd_max = 0;
   for (int i = 0; i < node_number; i++) {
     if (mtd_node_number[i] > mtd_max) {
       mtd_max = mtd_node_number[i];
     }
   }
   
   // Start to fill the attacker parameters   
   fill_param_3d (fp_in, buff, nb_profiles, node_number, Sp);
   for (int i = 0; i < nb_profiles; i++) { 
     for (int j = 0; j < node_number; j++) { 
       for (int k = 0; k < mtd_node_number[j]; k++) { 
       }
     }
   }
   
   escape_comment(fp_in, fp_out, buff);
   fill_param_2d (fp_in, buff, nb_profiles, node_number, Whp);
   for (int i = 0; i < nb_profiles; i++) { 
     for (int j = 0; j < node_number; j++) { 
     }
   }

   escape_comment(fp_in, fp_out, buff);
   fill_param_2d (fp_in, buff, nb_profiles, node_number, Chp);  
   for (int i = 0; i < nb_profiles; i++) { 
     for (int j = 0; j < node_number; j++) { 
     }
   }

   for (int i = 0; i < node_number; i++) {
     for (int j = 0; j < mtd_max; j++) {
       Cqos[i][j] = 0;
     }
   }

   escape_comment(fp_in, fp_out, buff);
   fill_param_2d (fp_in, buff, nb_profiles, node_number, Wp);  
   for (int i = 0; i < nb_profiles; i++) { 
     for (int j = 0; j < node_number; j++) { 
     }
   }

   escape_comment(fp_in, fp_out, buff);
   fill_param_2d (fp_in, buff, node_number, mtd_max, Cqos);  
   for (int i = 0; i < node_number; i++) {
     for (int j = 0; j < mtd_max; j++) {
     }
   }

   /* ------ DEFENDER SIDE  ------ */
   
   // Start to convert values
   fprintf (fp_out, "#Defender reward matrices \n# Rnmpn\'m\'\n");
   for (int profil_n = 0; profil_n < nb_profiles; profil_n++) { 
     for (int node_d = 0; node_d < node_number; node_d++) {
       for (int mtd_d = 0; mtd_d < mtd_node_number[node_d]; mtd_d++) {
	 for (int node_a = 0; node_a < node_number; node_a++) {
	   for (int mtd_a = 0; mtd_a < mtd_node_number[node_a]; mtd_a++) {
	     if (node_d == node_a) {
	       if (mtd_d == mtd_a) {
		 fprintf (fp_out, "%d %d %d %d %d %g\n", profil_n, node_d , mtd_d, node_a, mtd_a, Sp[profil_n][node_d][mtd_d] * Wp[profil_n][node_d] );
	       }
	       else {
		 fprintf (fp_out, "%d %d %d %d %d %g\n", profil_n, node_d, mtd_d, node_a, mtd_a, 0.0 );
	       }
	     }
	     else {
	       fprintf (fp_out, "%d %d %d %d %d %g\n", profil_n, node_d, mtd_d, node_a, mtd_a, 0.0 );
	     }
	   }
	 }
       }
     }
   }

   
  fprintf (fp_out, "STOP \n# Cnmpn\'m\'p\n");
   for (int profil_n = 0; profil_n < nb_profiles; profil_n++) { 
     for (int node_d = 0; node_d < node_number; node_d++) {
       for (int mtd_d = 0; mtd_d < mtd_node_number[node_d]; mtd_d++) {
	 for (int node_a = 0; node_a < node_number; node_a++) {
	   for (int mtd_a = 0; mtd_a < mtd_node_number[node_a]; mtd_a++) {
	     fprintf (fp_out, "%d %d %d %d %d %g\n", profil_n, node_d , mtd_d, node_a, mtd_a, Cqos[node_d][mtd_d]);	
	   }
	 }
       }
     }
   }  

   /* ------ ATTACKER SIDE  ------ */


   fprintf (fp_out, "STOP\n#Attacker reward matrices \n# Rhnmpn\'m\'\n");
   for (int profil_n = 0; profil_n < nb_profiles; profil_n++) { 
     for (int node_d = 0; node_d < node_number; node_d++) {
       for (int mtd_d = 0; mtd_d < mtd_node_number[node_d]; mtd_d++) {
	 for (int node_a = 0; node_a < node_number; node_a++) {
	   for (int mtd_a = 0; mtd_a < mtd_node_number[node_a]; mtd_a++) {
	     if (node_d == node_a) {
	       if (mtd_d == mtd_a) {
		 fprintf (fp_out, "%d %d %d %d %d %g\n", profil_n, node_d, mtd_d, node_a, mtd_a, 0.0 );
	       }
	       else {
		 fprintf (fp_out, "%d %d %d %d %d %g\n", profil_n, node_d , mtd_d, node_a, mtd_a, Sp[profil_n][node_a][mtd_a] * Whp[profil_n][node_a] );
	       }

	     }
	     else {
	       fprintf (fp_out, "%d %d %d %d %d %g\n", profil_n, node_d , mtd_d, node_a, mtd_a, Sp[profil_n][node_a][mtd_a] * Whp[profil_n][node_a] );
	     }
	   }
	 }
       }
     }
   }


   
   fprintf (fp_out, "STOP \n# Chnmpn\'m\'p\n");
   for (int profil_n = 0; profil_n < nb_profiles; profil_n++) { 
     for (int node_d = 0; node_d < node_number; node_d++) {
       for (int mtd_d = 0; mtd_d < mtd_node_number[node_d]; mtd_d++) {
	 for (int node_a = 0; node_a < node_number; node_a++) {
	   for (int mtd_a = 0; mtd_a < mtd_node_number[node_a]; mtd_a++) {
	     fprintf (fp_out, "%d %d %d %d %d %g\n", profil_n, node_d , mtd_d, node_a, mtd_a, Chp[profil_n][node_d]);	
	   }
	 }
       }
     }
   }


   fprintf (fp_out, "STOP \n\n");     
   printf ("convert parameter in %s into  reward in file %s\n", argv[1], argv[2]);
   
   return 0;
}
