#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>



int main(int argc, char* argv[])
{  
   if (argc != 3) { 
     printf ("Wrong usage, 2 arg required :\n ./rewards_and_costs_generator.x \" nb profile\"  \"configuration file\" \n");  
     return -1; 
   } 

   FILE *fp; 

   srand(time(NULL));
   fp = fopen(argv[2], "w"); 
   if (fp == NULL) { 
     printf ("error opening file %s \n", argv[2]);  
     return -1;     
   }

   // The number of attacker profiles
   int profile_number = atoi(argv[1]);   
   printf ("Randomly generate scenario for %d attacker profiles and 10 nodes\n", profile_number);

   // Number of node 
   int node_number = 10;

   // Number of mtd for each node
   int mtd_node_number[10] = {3,2,2,2,3,2,2,2,2,2};

   // Large integer number
   int M = 123456;

   // Probability to encounter an attacker profile.
   double attacker_proba = 1.0/profile_number;

   fprintf (fp, "# Randomly generated scenario\n#\n#\n#Problem Name : \n%d attacker profiles randomly generated scenario\n", profile_number);
   // Initiate the constants of the game
   fprintf (fp, "# Attacker profile number\n%d\n# Node Number : \n%d\n", profile_number, node_number);
   fprintf (fp, "# Number of MTD per node \n");
   for (int i = 0 ; i < node_number ; i++) {
     fprintf (fp, "# Node %d  :\n%d\n", i, mtd_node_number[i]);
   }
   fprintf (fp, "# M value \n%d\n", M);
   fprintf (fp, "# Probability to encounter each attacker profile \n");
   for (int i = 0 ; i < profile_number ; i++) {
     fprintf (fp, "# Attacker profile %d  :\n%g\n", i, attacker_proba);
   }


   // Intiate the Matrix of the game
   // Defender side
   fprintf (fp, "# Defender reward matrices\n#Rnmp\n#profile_number - node_number - mtd_number - value\n");
   for (int i = 0 ; i < profile_number ; i++) {
     fprintf (fp, "# Attacker profile %d \n", i);	 
     for (int j = 0 ; j < node_number ; j++) {
       for (int k = 0 ; k < mtd_node_number[j] ; k++) {
	 fprintf (fp, "%d %d %d %g\n", i, j, k, ((float)rand()/(float)(RAND_MAX)) * 2.5);	 
       }
     }
   }
   fprintf (fp, "STOP\n#Rndp\n#profile_number - node_number - value\n");
   for (int i = 0 ; i < profile_number ; i++) {
     fprintf (fp, "# Attacker profile %d \n", i);	 
     for (int j = 0 ; j < node_number ; j++) {
       fprintf (fp, "%d %d %g\n", i, j, ((float)rand()/(float)(RAND_MAX)) * 2.5);	 
     }
   }
   fprintf (fp, "STOP\n#Cnmp\n#profile_number - node_number - mtd_number - value\n");
   for (int i = 0 ; i < profile_number ; i++) {
     fprintf (fp, "# Attacker profile %d \n", i);	 
     for (int j = 0 ; j < node_number ; j++) {
       for (int k = 0 ; k < mtd_node_number[j] ; k++) {
	 fprintf (fp, "%d %d %d %g\n", i, j, k, ((float)rand()/(float)(RAND_MAX)) * 2.5);	 
       }
     }
   }
   fprintf (fp, "STOP\n#Cndp\n#profile_number - node_number - value\n");
   for (int i = 0 ; i < profile_number ; i++) {
     fprintf (fp, "# Attacker profile %d \n", i);	 
     for (int j = 0 ; j < node_number ; j++) {
       fprintf (fp, "%d %d %g\n", i, j, ((float)rand()/(float)(RAND_MAX)) * 2.5);	 
     }
   }

   // Attacker side
   fprintf (fp, "STOP\n# Attacker reward matrices\n#Rhnmp\n#profile_number - node_number - mtd_number - value\n");
   for (int i = 0 ; i < profile_number ; i++) {
     fprintf (fp, "# Attacker profile %d \n", i);	 
     for (int j = 0 ; j < node_number ; j++) {
       for (int k = 0 ; k < mtd_node_number[j] ; k++) {
	 fprintf (fp, "%d %d %d %g\n", i, j, k, ((float)rand()/(float)(RAND_MAX)) * 2.5);	 
	 
       }
     }
   }
   fprintf (fp, "STOP\n#Rhndp\n#profile_number - node_number - value\n");
   for (int i = 0 ; i < profile_number ; i++) {
     fprintf (fp, "# Attacker profile %d \n", i);	 
     for (int j = 0 ; j < node_number ; j++) {
       fprintf (fp, "%d %d %g\n", i, j, ((float)rand()/(float)(RAND_MAX)) * 2.5);	 
     }
   }

   fprintf (fp, "STOP\n#Chnmp\n#profile_number - node_number - mtd_number - value\n");
   for (int i = 0 ; i < profile_number ; i++) {
     fprintf (fp, "# Attacker profile %d \n", i);	 
     for (int j = 0 ; j < node_number ; j++) {
       for (int k = 0 ; k < mtd_node_number[j] ; k++) {
	 fprintf (fp, "%d %d %d %g\n", i, j, k, ((float)rand()/(float)(RAND_MAX)) * 2.5);	 
       }
     }
   }

   fprintf (fp, "STOP\n#Chndp\n#profile_number - node_number - value\n");
   for (int i = 0 ; i < profile_number ; i++) {
     fprintf (fp, "# Attacker profile %d \n", i);	 
     for (int j = 0 ; j < node_number ; j++) {
       fprintf (fp, "%d %d %g\n", i, j, ((float)rand()/(float)(RAND_MAX)) * 2.5);	 
     }
   }

   
   
   return 0;
}
