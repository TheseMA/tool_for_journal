#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char* argv[])
{  
  if (argc != 2) { 
    printf ("Wrong usage, 1 arg required :\n ./plot_solver_results.x \" nb profile\"  \n");  
    return -1; 
  } 
  // The number of attacker profiles
  int profile_number = atoi(argv[1]);
  
  FILE *fp; 
  fp = fopen("results/plot.plt", "w"); 
  
  if (fp == NULL) { 
    printf ("error opening file %s \n", "results/plot_cplex.plt");  
    return -1;     
  }

  fprintf(fp,"set terminal pngcairo \nset output  'plot_glpk.png'\nset xlabel 'nb profiles'\nset ylabel 'time to compute in s'\nset grid\nplot 'data_cplex.plt' using 1:2  title 'time taken to compute the solution - timeout : 10min'");
  fclose(fp);

  
  fp = fopen("results/data_cplex.plt", "w"); 
  if (fp == NULL) { 
    printf ("error opening file %s \n", "results/plot.plt");  
    return -1;     
  }
  fprintf(fp, "# Profile number - used MTD\n");
  
  FILE *fg;
  char name[40] =  "generated_sc/scenario1/dump\0";
  char buff[255];

  fprintf(fp, "0 0\n");      
  for (int i = 1 ; i <= profile_number ; i++) {    
    if (i < 10) {    
      name[21] = i + '0' ;
      printf("%s\n",name);
    }
    else if (i < 100) {    
      name[21] = i/10 + '0' ;
      name[22] = i%10 + '0' ;
      sprintf(name+23, "%s", "/dump\0");
      printf("%s\n",name);
    }
    else {
      name[21] = i/100 + '0' ;
      name[22] = (i-((i/100)*100) )/10 + '0' ;
      name[23] = i%10 + '0' ;
      sprintf(name+24, "%s", "/dump\0");
      printf("%s\n",name);
    }
    fg = fopen(name, "r");   
    if (fg == NULL) { 
      printf ("error opening dump file for scenario_%d \n", i);  
      return -1;     
    }
    printf ("in file %s\n", name);
    
    while (fscanf(fg, "%s", buff) != EOF) {
      if (strcmp (buff, "user") == 0) {
	fscanf(fg, "%s", buff);
        if (buff[1] == 'm') {
	  int min = (int)buff[0] - 48;
	  int sec = (int)buff[2] - 48;
	  sec += min*60;
	  // If less than 10 sec
	  if (buff[3] == ',') {
	    fprintf(fp, "%d %d%c%c%c\n", i, sec, buff[4], buff[5], buff[6]);
	  }
	  else {
	    fprintf(fp, "%d %d%c%c%c%c\n", i, sec, buff[3], buff[5], buff[6], buff[7]);
	  }	  
	}

	// If more than 10 sec
	else if (buff[2] == 'm') {

	  int min = ((int)buff[0]- 48) * 10 ;
	  min += (int)buff[1] - 48;
	  int sec = (int)buff[3]- 48;
	  sec += min*60;
	  // If less than 10 sec
	  if (buff[4] == ',') {
	    fprintf(fp, "%d %d%c%c%c\n", i, sec, buff[5], buff[6], buff[7]);
	  }
	  else {
	    fprintf(fp, "%d %d%c%c%c%c\n", i, sec, buff[4], buff[6], buff[7], buff[8]);
	  }	  
	}       
      }
    }
    fclose(fg);
  }
  
  fprintf(fp, "%d 0\n", profile_number);   
  fclose(fp);
  return 0;
}
