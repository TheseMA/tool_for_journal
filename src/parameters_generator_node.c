#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <limits.h>
#include <float.h>

#define _PI 3.1415927

#define LENGTH 1000

int mtd_node_number[LENGTH];
double attacker_proba[LENGTH];

int main(int argc, char* argv[])
{  
   if (argc != 4) { 
     printf ("Wrong usage, 3 arg required :\n ./parameters_generator.x \" nb profile\" \" nb node\"  \"configuration file\" \n");  
     return -1; 
   } 

   srand(time(NULL));
      
   FILE *fp; 
   fp = fopen(argv[3], "w"); 
   if (fp == NULL) { 
     printf ("error opening file %s \n", argv[3]);  
     return -1;     
   }

   // The number of attacker profiles
   int profile_number = atoi(argv[1]);
   printf ("Randomly generate scenario for %d attacker profiles\n", profile_number);
   // Number of node 
   int node_number = atoi(argv[2]);

   for (int i = 0 ; i < node_number ; i++) {
     mtd_node_number[i] = 3;
   }
   
   // Large integer number
   double M = 2147483647;   
   double m = profile_number / 2;
   double sum = 0;
   // Probability to encounter an attacker profile.

   for (int x = 0 ; x < profile_number ; x++) {
     attacker_proba[x] = (1/(sqrt(2 * _PI * ((profile_number/5)*(profile_number/5))))) * exp(- ( ((x+1-m)*(x+1-m)) / (2*((profile_number/5)*(profile_number/5))) ) );
     sum += attacker_proba[x]; 
   }

   fprintf (fp, "# Randomly generated scenario\n#\n#\n#Problem Name : \n%d attacker profiles randomly generated scenario\n", profile_number);
   // Initiate the constants of the game   
   fprintf (fp, "# Attacker profile number\n%d\n# Node Number : \n%d\n", profile_number, node_number);
   fprintf (fp, "# Number of MTD per node \n");
   
   for (int i = 0 ; i < node_number ; i++) {
     fprintf (fp, "# Node %d  :\n%d\n", i, mtd_node_number[i]);
   }
   fprintf (fp, "# M value \n%lf\n", M);
   fprintf (fp, "# Probability to encounter each attacker profile \n");

   if (profile_number > 10) {
     for (int i = 0 ; i < profile_number ; i++) {
       fprintf (fp, "# Attacker profile %d  :\n%.10f\n", i, attacker_proba[i]);
     }
   }
   else {
     for (int i = 0 ; i < profile_number ; i++) {
       fprintf (fp, "# Attacker profile %d  :\n%.10f\n", i, 1.0/profile_number);
     }
   }

   // Intiate the Matrix of the game

   /* ------ Attacker side -------*/
   fprintf (fp, "# Attacker parameters \n# Sp\n#profile_number - node_number - mtd_nb - value\n");
   for (int i = 0 ; i < profile_number ; i++) {
     //fprintf (fp, "# Attacker profile %d \n", i);	 
     for (int j = 0 ; j < node_number ; j++) {
       for (int k = 0 ; k < mtd_node_number[j] ; k++) {
	 fprintf (fp, "%d %d %d %g\n", i, j, k, ((float)rand()/(float)(RAND_MAX)) * 1);	 
       }
     }
   }

   fprintf (fp, "STOP\n#Whp\n#profile_number - node_number - value\n");
   for (int i = 0 ; i < profile_number ; i++) {
     for (int j = 0 ; j < node_number ; j++) {
       fprintf (fp, "%d %d %g\n", i, j, ((float)rand()/(float)(RAND_MAX)) * 5000);	 
     }
   }

   fprintf (fp, "STOP\n#Chp\n#profile_number - node_number - value\n");
   for (int i = 0 ; i < profile_number ; i++) {
     for (int j = 0 ; j < node_number ; j++) {
       fprintf (fp, "%d %d %g\n", i, j, ((float)rand()/(float)(RAND_MAX)) * 15);	 
     }
   }

   /* ------ Defender side -------*/
   
   fprintf (fp, "STOP\n# Defender parameters \n#Wp\n#profile_number - node_number - value\n");
   for (int i = 0 ; i < profile_number ; i++) {
     for (int j = 0 ; j < node_number ; j++) {
       fprintf (fp, "%d %d %g\n", i, j, ((float)rand()/(float)(RAND_MAX)) * 5000);	 
     }
   }

   fprintf (fp, "STOP\n#CQoS\n# node_number- mtd_node_number - value\n");
   for (int j = 0 ; j < node_number ; j++) {
     for (int k = 0 ; k < mtd_node_number[j] ; k++) {
       fprintf (fp, "%d %d %g\n", j, k, ((float)rand()/(float)(RAND_MAX)) * 15);	 
     }
   }
   

   fprintf (fp, "STOP\n\n");
   
   return 0;
}
