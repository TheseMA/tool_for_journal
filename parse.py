#!/usr/bin/python

import sys

if len(sys.argv) != 2 :

    print "wrong usage : parse.py input_file"
    sys.exit()
    
    
# file = open('sol_essay.sol');

file = open(sys.argv[1]);

file_c = file.read();

toto = file_c.split("<variables>")

toto2 = toto[1].split("</variables>")

var = toto2[0]

length = len(toto);

# The defender strategy
w, h =  3, 10
delta = [[0 for x in range(w)] for y in range(h)]
alpha = [[[0 for x in range(w)] for y in range(h)] for j in range(h)]

delta_idl = [0 for y in range(h)]
#alpha_d = [[0 for y in range(h)] for u in range(h)]

# Initiate the tabs at 0
for i in range(10) :
    for j in range(3) :
        delta[i][j] = 0.;
        for k in range(10):
            alpha[k][i][j] = 0.;
    delta_idl[i] = 0.;
 #   for k in range(10):
#        alpha_d[k][i] = 0.;


# Extract the strategies 

# Get a var2 tab case per variable name 
var2 = var.split("/>")

for i in range (len(var2)) :

    line = var2[i].split('"')
    
    if len(line) > 1 :

        # If it's a Z
        if line[1][0 ]== 'Z' :
            # Is it a Znm ?
            if line[1][13] == 'p' :
#                if line[1][18] == '0' :
                #extract node and mtdnumber
                delta[int(line[1][6])][int(line[1][12])] += float(line[5])                

        # If it's a alpha
        if line[1][0] == 'a' :
            # Is it a alpha_nm ?
            if line[1][18] == 'm' :
                alpha[int(line[1][11])][int(line[1][17])][int(line[1][23])] += float(line[5])          

        # If it's idle
        if line[1][0]== 'I' :
            delta_idl[int(line[1][10])] += float(line[5])

            
# Print the strategy for attacker and defender     

print ('Defender Strategy');
    
for i in range(10):
    for j in range(3) :
        print ('\\delta_{n' + str(i) + '_m' + str(j) + '} = ' + str(delta[i][j]));
    print ('\\delta_{n' + str(i) + 'idl} = ' + str(delta_idl[i]));

print ('\n\nAttacker Strategy');
    
for u in range(10):
    for i in range(10):
        for j in range(3) :
            if alpha[u][i][j] != 0 :
                print ('\\alpha_{p' + str(u) + '_n' + str(i) + '_m' + str(j) + '} = ' + str(alpha[u][i][j]));
#        if alpha_d[u][i] != 0 :
#            print ('alphad_p' + str(u) + '_n' + str(i) + ' = ' + str(alpha_d[u][i]));
    
